1
00:00:00,160 --> 00:00:05,600
stack buffer overflows what are those

2
00:00:02,800 --> 00:00:07,279
well it's when too much ACID data is

3
00:00:05,600 --> 00:00:09,440
copied into a buffer that's stored on

4
00:00:07,279 --> 00:00:12,080
the stack overflowing its bounds and

5
00:00:09,440 --> 00:00:14,080
subsequently corrupting other data so

6
00:00:12,080 --> 00:00:15,920
for instance if you had a memcpy where

7
00:00:14,080 --> 00:00:18,240
you had a attacker controlled length and

8
00:00:15,920 --> 00:00:20,400
attacker controlled data going into some

9
00:00:18,240 --> 00:00:22,240
particular vulnerable buffer because the

10
00:00:20,400 --> 00:00:24,160
attacker controls the length they can

11
00:00:22,240 --> 00:00:26,160
guarantee that they can overflow the

12
00:00:24,160 --> 00:00:28,080
vulnerable buffer but before we get too

13
00:00:26,160 --> 00:00:29,920
much into this we need to establish some

14
00:00:28,080 --> 00:00:31,679
conventions that we're going to use for

15
00:00:29,920 --> 00:00:33,440
diagrams in this class

16
00:00:31,679 --> 00:00:36,079
if you see gray that is meant to

17
00:00:33,440 --> 00:00:39,040
indicate it is uninitialized data blue

18
00:00:36,079 --> 00:00:41,280
is initialized red is ACID attacker

19
00:00:39,040 --> 00:00:43,520
controlled input data and blue

20
00:00:41,280 --> 00:00:47,120
transitioning into red means that it's

21
00:00:43,520 --> 00:00:49,440
semi attacker control data or SACI data

22
00:00:47,120 --> 00:00:51,600
now you will see that there are frequent

23
00:00:49,440 --> 00:00:53,760
cases where an attacker may not exactly

24
00:00:51,600 --> 00:00:55,840
control everything about the data but

25
00:00:53,760 --> 00:00:57,920
still even with SACI data instead of

26
00:00:55,840 --> 00:00:59,840
ACID data an attacker can still achieve

27
00:00:57,920 --> 00:01:02,239
their goals most of the time

28
00:00:59,840 --> 00:01:04,640
so if this was 64 bytes then when we're

29
00:01:02,239 --> 00:01:06,640
drawing it as a stack diagram we will

30
00:01:04,640 --> 00:01:08,560
place low address as low and high

31
00:01:06,640 --> 00:01:10,240
address is high this is a convention

32
00:01:08,560 --> 00:01:12,799
that's common in computer architecture

33
00:01:10,240 --> 00:01:14,080
books and is also used in other OST2

34
00:01:12,799 --> 00:01:15,840
classes

35
00:01:14,080 --> 00:01:17,759
now in this convention if we assume that

36
00:01:15,840 --> 00:01:19,920
we've got a little endian architecture

37
00:01:17,759 --> 00:01:22,080
we're going to draw the little end or

38
00:01:19,920 --> 00:01:24,320
the least significant byte here to the

39
00:01:22,080 --> 00:01:26,400
right and the big end or most

40
00:01:24,320 --> 00:01:28,240
significant byte to the left

41
00:01:26,400 --> 00:01:30,560
okay well we know that a stack data

42
00:01:28,240 --> 00:01:32,079
structure has data growing from the

43
00:01:30,560 --> 00:01:34,320
bottom of the stack to the top of the

44
00:01:32,079 --> 00:01:36,240
stack and therefore with our low dresses

45
00:01:34,320 --> 00:01:38,159
low and high addresses high convention

46
00:01:36,240 --> 00:01:40,799
we're going to be seeing the stack

47
00:01:38,159 --> 00:01:42,399
growing downward so eight bytes at a

48
00:01:40,799 --> 00:01:44,079
time for instance on a 64-bit

49
00:01:42,399 --> 00:01:45,840
architecture are typically pushed onto

50
00:01:44,079 --> 00:01:48,560
the stack and that means it's going to

51
00:01:45,840 --> 00:01:50,799
grow towards progressively lower things

52
00:01:48,560 --> 00:01:52,720
and the thing at the lowest address is

53
00:01:50,799 --> 00:01:54,880
the top of the stack and the thing at

54
00:01:52,720 --> 00:01:55,920
the top of the diagram is the bottom of

55
00:01:54,880 --> 00:01:57,920
the stack

56
00:01:55,920 --> 00:02:01,360
all right so stack grows towards low

57
00:01:57,920 --> 00:02:02,719
addresses but memory rights grow towards

58
00:02:01,360 --> 00:02:04,640
high addresses

59
00:02:02,719 --> 00:02:07,200
so any sort of right is going to

60
00:02:04,640 --> 00:02:09,840
overwrite this direction going up on

61
00:02:07,200 --> 00:02:13,599
this diagram in this orientation

62
00:02:09,840 --> 00:02:14,560
so for instance if we had a 64 byte

63
00:02:13,599 --> 00:02:16,480
buffer

64
00:02:14,560 --> 00:02:19,680
the indexing which is going to be used

65
00:02:16,480 --> 00:02:22,879
on our stack diagrams is index 0 and x1

66
00:02:19,680 --> 00:02:24,800
etc all the way up through index 63 up

67
00:02:22,879 --> 00:02:28,080
here at the top

68
00:02:24,800 --> 00:02:29,760
and so if we had some value like hex one

69
00:02:28,080 --> 00:02:32,160
one two two three three four four

70
00:02:29,760 --> 00:02:33,440
etcetera if this was a little endian

71
00:02:32,160 --> 00:02:35,200
architecture meaning the least

72
00:02:33,440 --> 00:02:37,120
significant byte is here on the right

73
00:02:35,200 --> 00:02:39,519
and the most significant byte is here on

74
00:02:37,120 --> 00:02:41,120
the left then we would convert to this

75
00:02:39,519 --> 00:02:43,680
eight individual bytes if we were

76
00:02:41,120 --> 00:02:45,760
interpreting it as a keyword or a

77
00:02:43,680 --> 00:02:47,360
one signed long long

78
00:02:45,760 --> 00:02:49,040
that would be interpreted as an eight

79
00:02:47,360 --> 00:02:50,640
byte value of hex one two three four

80
00:02:49,040 --> 00:02:52,800
five etcetera

81
00:02:50,640 --> 00:02:54,959
so you know this is just uh trying to

82
00:02:52,800 --> 00:02:56,879
remind us you know how little endianness

83
00:02:54,959 --> 00:02:58,720
works on architecture

84
00:02:56,879 --> 00:03:01,440
on the other hand if you had something

85
00:02:58,720 --> 00:03:03,040
like a string instead of a number well

86
00:03:01,440 --> 00:03:07,200
the least significant byte of a string

87
00:03:03,040 --> 00:03:08,319
is over here so slash h o m e slash u s

88
00:03:07,200 --> 00:03:10,480
e r

89
00:03:08,319 --> 00:03:13,360
so this would for instance be the string

90
00:03:10,480 --> 00:03:15,920
slash home slash user slash sbo

91
00:03:13,360 --> 00:03:18,159
so again the little endian-ness means

92
00:03:15,920 --> 00:03:20,720
that it's going to go from the right to

93
00:03:18,159 --> 00:03:22,400
the left bottom to the top in stack

94
00:03:20,720 --> 00:03:24,000
diagrams

95
00:03:22,400 --> 00:03:25,680
now when it comes to what's going on

96
00:03:24,000 --> 00:03:27,519
behind the scenes there's a variety of

97
00:03:25,680 --> 00:03:29,840
things that can be stored on the stack

98
00:03:27,519 --> 00:03:32,159
and it's going to depend on the computer

99
00:03:29,840 --> 00:03:33,920
architecture the calling conventions

100
00:03:32,159 --> 00:03:36,080
that are used by the particular

101
00:03:33,920 --> 00:03:38,400
compilers and code that's being compiled

102
00:03:36,080 --> 00:03:40,959
and we cover this in other classes such

103
00:03:38,400 --> 00:03:42,560
as assembly classes for purposes of this

104
00:03:40,959 --> 00:03:44,400
class the only thing that you really

105
00:03:42,560 --> 00:03:46,560
really need to know is the fact that

106
00:03:44,400 --> 00:03:48,480
there is a return address generally

107
00:03:46,560 --> 00:03:50,640
stored on the stack somewhere on most

108
00:03:48,480 --> 00:03:52,480
architectures not necessarily in leaf

109
00:03:50,640 --> 00:03:53,920
functions but pretty much every

110
00:03:52,480 --> 00:03:55,680
architecture is going to have something

111
00:03:53,920 --> 00:03:58,720
like a return address and the return

112
00:03:55,680 --> 00:04:01,040
address is the address of code where

113
00:03:58,720 --> 00:04:04,400
code should return when you return out

114
00:04:01,040 --> 00:04:06,480
of a C function right so in C we call a

115
00:04:04,400 --> 00:04:08,319
function in order to jump in and start

116
00:04:06,480 --> 00:04:10,799
executing the code and when we're done

117
00:04:08,319 --> 00:04:14,000
with function we return back from that

118
00:04:10,799 --> 00:04:15,760
function and so somewhere somehow the

119
00:04:14,000 --> 00:04:17,680
architecture has to store like how do we

120
00:04:15,760 --> 00:04:19,919
get back to the place that we came from

121
00:04:17,680 --> 00:04:21,840
when this call was issued and we call

122
00:04:19,919 --> 00:04:23,280
that the return address which is stored

123
00:04:21,840 --> 00:04:25,680
on the stack

124
00:04:23,280 --> 00:04:27,759
so if the stack is organized into like a

125
00:04:25,680 --> 00:04:30,080
sequence of frames for function one

126
00:04:27,759 --> 00:04:32,720
calls function two calls function three

127
00:04:30,080 --> 00:04:34,639
then on our stack diagrams the frames

128
00:04:32,720 --> 00:04:36,639
for earlier functions will be on the top

129
00:04:34,639 --> 00:04:39,040
and later functions and ultimately the

130
00:04:36,639 --> 00:04:40,560
leaf function will be on the bottom and

131
00:04:39,040 --> 00:04:42,160
so again there's a variety of things

132
00:04:40,560 --> 00:04:44,240
that could be on the stack and it really

133
00:04:42,160 --> 00:04:46,000
all just depends but what I want to say

134
00:04:44,240 --> 00:04:48,000
is that return address is going to be

135
00:04:46,000 --> 00:04:49,680
the very key and critical thing this is

136
00:04:48,000 --> 00:04:51,919
what attackers are going to be trying to

137
00:04:49,680 --> 00:04:53,759
corrupt with stack buffer overflows but

138
00:04:51,919 --> 00:04:55,680
there's also things like local variables

139
00:04:53,759 --> 00:04:57,680
and attackers can get a lot of benefit

140
00:04:55,680 --> 00:05:00,160
from corrupting local variables

141
00:04:57,680 --> 00:05:03,039
there's saved registers in terms of

142
00:05:00,160 --> 00:05:04,880
callee and caller saved registers

143
00:05:03,039 --> 00:05:06,479
there's function parameters

144
00:05:04,880 --> 00:05:08,080
on some architectures which might get

145
00:05:06,479 --> 00:05:10,240
saved on the stack and it could depend

146
00:05:08,080 --> 00:05:12,240
on how many parameters are cut passed to

147
00:05:10,240 --> 00:05:14,000
a function and so forth

148
00:05:12,240 --> 00:05:16,160
and then you know again return addresses

149
00:05:14,000 --> 00:05:18,639
so this is the type of things that we

150
00:05:16,160 --> 00:05:20,400
could have I left out various things

151
00:05:18,639 --> 00:05:21,840
depending on architecture but in general

152
00:05:20,400 --> 00:05:23,520
all architectures are going to have this

153
00:05:21,840 --> 00:05:25,440
kind of stuff and the one we really

154
00:05:23,520 --> 00:05:27,120
really care about is that return address

155
00:05:25,440 --> 00:05:28,320
that's going to be a good target for an

156
00:05:27,120 --> 00:05:30,320
attacker

157
00:05:28,320 --> 00:05:31,919
so those are our conventions let's get

158
00:05:30,320 --> 00:05:35,360
back to it what is a stack buffer

159
00:05:31,919 --> 00:05:37,680
overflow it's when too much ACID data is

160
00:05:35,360 --> 00:05:39,039
copied into a stack buffer overflowing

161
00:05:37,680 --> 00:05:41,280
its bounds

162
00:05:39,039 --> 00:05:43,600
so let's you know let's consider this

163
00:05:41,280 --> 00:05:45,440
you know visualize this let's say we had

164
00:05:43,600 --> 00:05:47,759
a memcpy and it's

165
00:05:45,440 --> 00:05:50,320
copying into a vulnerable buffer it's

166
00:05:47,759 --> 00:05:52,400
copying from ACID buffer and it has an

167
00:05:50,320 --> 00:05:54,160
attacker controlled length so this would

168
00:05:52,400 --> 00:05:56,319
look something like this you're copying

169
00:05:54,160 --> 00:05:58,560
the data from here and it's flowing and

170
00:05:56,319 --> 00:06:00,720
it's flowing and eventually it's

171
00:05:58,560 --> 00:06:03,039
overflowing the bounds of the vulnerable

172
00:06:00,720 --> 00:06:05,280
buffer and it's consequently smashing

173
00:06:03,039 --> 00:06:07,440
the return address on the stack and that

174
00:06:05,280 --> 00:06:08,960
gives the attacker some benefit they

175
00:06:07,440 --> 00:06:11,199
overwrite the return address and

176
00:06:08,960 --> 00:06:13,120
therefore when the function returns it's

177
00:06:11,199 --> 00:06:14,639
going to go to some attacker controlled

178
00:06:13,120 --> 00:06:16,720
location

179
00:06:14,639 --> 00:06:19,039
so what are the common causes of stack

180
00:06:16,720 --> 00:06:21,039
buffer overflows well we've got two root

181
00:06:19,039 --> 00:06:22,880
causes we've got the sweet potato and

182
00:06:21,039 --> 00:06:25,039
the carrot these being the only roots

183
00:06:22,880 --> 00:06:27,440
available in the emoji set at the time

184
00:06:25,039 --> 00:06:29,520
so in the sweet potato case we've got

185
00:06:27,440 --> 00:06:31,520
unsafe or weakly bounded common

186
00:06:29,520 --> 00:06:34,160
functions like memcpy strcpy

187
00:06:31,520 --> 00:06:36,160
string cat s printf etc these are things

188
00:06:34,160 --> 00:06:38,080
that typically you know are either

189
00:06:36,160 --> 00:06:40,400
explicitly for purposes of copying

190
00:06:38,080 --> 00:06:42,319
memory or you know implicitly doing

191
00:06:40,400 --> 00:06:44,240
things like string operations which will

192
00:06:42,319 --> 00:06:46,319
lead to copying memory

193
00:06:44,240 --> 00:06:48,319
there's also very frequently

194
00:06:46,319 --> 00:06:50,560
wrappers where you know some programmer

195
00:06:48,319 --> 00:06:52,319
wants to do a custom memcpy that's

196
00:06:50,560 --> 00:06:54,319
just for their particular struct data

197
00:06:52,319 --> 00:06:56,160
type or something like that and at the

198
00:06:54,319 --> 00:06:58,720
end of the day they may be copying one

199
00:06:56,160 --> 00:07:00,160
struct to another struct but behind the

200
00:06:58,720 --> 00:07:01,919
scenes you know it's usually backed by

201
00:07:00,160 --> 00:07:03,360
something like a memcpy so you have to

202
00:07:01,919 --> 00:07:05,599
watch out for these sort of wrapper

203
00:07:03,360 --> 00:07:07,599
functions as well

204
00:07:05,599 --> 00:07:10,000
then we've got the other carrot cause

205
00:07:07,599 --> 00:07:13,440
which is sequential data rights within a

206
00:07:10,000 --> 00:07:15,520
loop that is an asset exit condition so

207
00:07:13,440 --> 00:07:17,039
data rights so right here means like

208
00:07:15,520 --> 00:07:19,360
it's copying it's copying from one

209
00:07:17,039 --> 00:07:20,960
location to a different memory location

210
00:07:19,360 --> 00:07:22,720
usually not with something like a mem

211
00:07:20,960 --> 00:07:24,720
copy usually something just like you

212
00:07:22,720 --> 00:07:26,400
know assigning to

213
00:07:24,720 --> 00:07:28,800
some dereferenced pointer or something

214
00:07:26,400 --> 00:07:30,960
like that so you've got rights and

215
00:07:28,800 --> 00:07:33,680
they're within a loop so it effectively

216
00:07:30,960 --> 00:07:35,840
is a memory copy operation and when that

217
00:07:33,680 --> 00:07:37,199
loop exit condition is attacker

218
00:07:35,840 --> 00:07:38,880
controlled that's when you're going to

219
00:07:37,199 --> 00:07:40,560
get into problems because they can

220
00:07:38,880 --> 00:07:43,680
potentially control that loop to keep

221
00:07:40,560 --> 00:07:44,639
copying too much and ultimately overflow

222
00:07:43,680 --> 00:07:46,479
buffer

223
00:07:44,639 --> 00:07:47,919
so let's look at a trivial example of

224
00:07:46,479 --> 00:07:49,440
this we're going to be going into some

225
00:07:47,919 --> 00:07:51,599
real examples later on but you got to

226
00:07:49,440 --> 00:07:53,120
always start with the trivial example

227
00:07:51,599 --> 00:07:55,759
so here we've got our sbo

228
00:07:53,120 --> 00:07:57,360
stackbufferoverflow.c

229
00:07:55,759 --> 00:08:00,319
and let's say we have

230
00:07:57,360 --> 00:08:01,919
main and it's got an rgc and an rv rv is

231
00:08:00,319 --> 00:08:04,000
the argument vector it's the command

232
00:08:01,919 --> 00:08:06,479
line arguments passed in on the command

233
00:08:04,000 --> 00:08:09,120
line we've got an 8 byte buffer on the

234
00:08:06,479 --> 00:08:11,360
stack and then we've got a strcpy

235
00:08:09,120 --> 00:08:13,840
which is copying from rdv1 the first

236
00:08:11,360 --> 00:08:15,280
command line parameter into buff and

237
00:08:13,840 --> 00:08:17,280
then it prints out you know we got the

238
00:08:15,280 --> 00:08:19,520
following input string and it's you know

239
00:08:17,280 --> 00:08:20,879
supposed to just take some very simple

240
00:08:19,520 --> 00:08:23,759
you know hopefully less than eight

241
00:08:20,879 --> 00:08:26,479
character input but obviously if an

242
00:08:23,759 --> 00:08:28,560
attacker is able to feed in tucker

243
00:08:26,479 --> 00:08:30,479
control data here as the first command

244
00:08:28,560 --> 00:08:32,640
line parameter they can feed in an

245
00:08:30,479 --> 00:08:35,440
arbitrary sized input and that will

246
00:08:32,640 --> 00:08:37,279
consequently overflow the bounds of buff

247
00:08:35,440 --> 00:08:38,399
so let's visualize that here's our stack

248
00:08:37,279 --> 00:08:40,479
diagram

249
00:08:38,399 --> 00:08:42,640
and we're going to say here's argv of

250
00:08:40,479 --> 00:08:45,600
zero and we'll say it's slash home slash

251
00:08:42,640 --> 00:08:47,600
user slash sbo rv of zero is you know of

252
00:08:45,600 --> 00:08:49,680
course usually by convention it's the

253
00:08:47,600 --> 00:08:53,040
path to the executable itself when it's

254
00:08:49,680 --> 00:08:54,959
run and I've shown it here as SACI data

255
00:08:53,040 --> 00:08:56,880
instead of fully attack control data

256
00:08:54,959 --> 00:08:58,080
just because I'm you know nitpicking and

257
00:08:56,880 --> 00:09:00,160
nerding out you've got to be very

258
00:08:58,080 --> 00:09:02,080
precise with these kind of things

259
00:09:00,160 --> 00:09:04,000
you know this is SACI data because

260
00:09:02,080 --> 00:09:05,920
we're not presuming that the attacker

261
00:09:04,000 --> 00:09:07,279
has complete control over the exact

262
00:09:05,920 --> 00:09:10,640
string because that would imply they

263
00:09:07,279 --> 00:09:12,720
could execute this binary from any path

264
00:09:10,640 --> 00:09:13,839
and usually on most unix type systems

265
00:09:12,720 --> 00:09:15,600
that would imply that they've already

266
00:09:13,839 --> 00:09:17,440
got administrative privileges which on

267
00:09:15,600 --> 00:09:19,760
most unix systems already implies they

268
00:09:17,440 --> 00:09:21,600
have complete control of the system

269
00:09:19,760 --> 00:09:24,560
anyways rxv

270
00:09:21,600 --> 00:09:27,600
home slash user slash sbl on my system

271
00:09:24,560 --> 00:09:30,320
and it's SACI data then arcv of one

272
00:09:27,600 --> 00:09:31,760
that's our SACI string now this is the

273
00:09:30,320 --> 00:09:33,360
attack controlled input and you'd think

274
00:09:31,760 --> 00:09:34,880
that you know the attacker can provide

275
00:09:33,360 --> 00:09:36,480
anything they want on the command line

276
00:09:34,880 --> 00:09:38,080
and that is technically true and you

277
00:09:36,480 --> 00:09:40,560
know of course you can call things like

278
00:09:38,080 --> 00:09:42,800
exec to invoke this program with you

279
00:09:40,560 --> 00:09:45,600
know just a truly arbitrary thing but

280
00:09:42,800 --> 00:09:48,959
I've again drawn it as SACI data rather

281
00:09:45,600 --> 00:09:51,760
than ACID data in order to indicate that

282
00:09:48,959 --> 00:09:53,360
because this is going to be going into a

283
00:09:51,760 --> 00:09:55,920
strcpy

284
00:09:53,360 --> 00:09:59,040
the strcpy will not copy arbitrary

285
00:09:55,920 --> 00:10:01,920
bytes it will copy string by its typical

286
00:09:59,040 --> 00:10:04,320
valid ascii string types

287
00:10:01,920 --> 00:10:06,720
type of data and so consequently the

288
00:10:04,320 --> 00:10:08,959
attacker is not going to set these byte

289
00:10:06,720 --> 00:10:11,200
values to anything they want they are

290
00:10:08,959 --> 00:10:13,440
going to have to account for the fact

291
00:10:11,200 --> 00:10:15,440
that only cop only bytes that string

292
00:10:13,440 --> 00:10:17,040
copy considers valid will actually be

293
00:10:15,440 --> 00:10:18,720
copied and therefore that will add

294
00:10:17,040 --> 00:10:20,640
constraints to the value and that's why

295
00:10:18,720 --> 00:10:21,839
it's not fully attacker controlled it's

296
00:10:20,640 --> 00:10:24,079
SACI

297
00:10:21,839 --> 00:10:25,600
so here's our code and so let's say that

298
00:10:24,079 --> 00:10:28,480
you know what does the stack look like

299
00:10:25,600 --> 00:10:30,880
uh before main is actually executed okay

300
00:10:28,480 --> 00:10:32,480
we've got argv we've got arg1 some sort

301
00:10:30,880 --> 00:10:33,279
of you know maybe initialized data in

302
00:10:32,480 --> 00:10:36,880
there

303
00:10:33,279 --> 00:10:38,880
main is then called and the call to main

304
00:10:36,880 --> 00:10:41,120
is going to push a return address onto

305
00:10:38,880 --> 00:10:43,360
the stack typically main is called by

306
00:10:41,120 --> 00:10:45,519
some other sort of helper you know

307
00:10:43,360 --> 00:10:48,240
component before main so you may write

308
00:10:45,519 --> 00:10:50,079
main but something within you know the

309
00:10:48,240 --> 00:10:51,920
system runtime is going to actually call

310
00:10:50,079 --> 00:10:54,320
your main and that is going to push a

311
00:10:51,920 --> 00:10:55,920
return address back out of main onto the

312
00:10:54,320 --> 00:10:58,560
stack

313
00:10:55,920 --> 00:11:00,480
then we have buff 8 which is allocated

314
00:10:58,560 --> 00:11:03,440
inside of the main code and so here's

315
00:11:00,480 --> 00:11:05,440
our eight bytes for buff

316
00:11:03,440 --> 00:11:07,600
and then finally the strcpy is

317
00:11:05,440 --> 00:11:10,720
called and so strcpy is copying

318
00:11:07,600 --> 00:11:13,680
from argv of one to buff well argue of

319
00:11:10,720 --> 00:11:15,920
one here is depicted as bigger than buff

320
00:11:13,680 --> 00:11:19,040
and consequently that is going to

321
00:11:15,920 --> 00:11:21,200
overflow and corrupt the saved return

322
00:11:19,040 --> 00:11:24,560
address on the stack and now all of a

323
00:11:21,200 --> 00:11:27,040
sudden when main returns back out to the

324
00:11:24,560 --> 00:11:29,839
thing that call domain it's going to

325
00:11:27,040 --> 00:11:31,839
return to a semi-arbitrary attacker

326
00:11:29,839 --> 00:11:33,120
controlled value and so you know we

327
00:11:31,839 --> 00:11:35,200
don't know exactly where that's going to

328
00:11:33,120 --> 00:11:38,320
be but it is incumbent upon the attacker

329
00:11:35,200 --> 00:11:40,079
to figure out an advantageous place to

330
00:11:38,320 --> 00:11:41,600
cause it to return to

331
00:11:40,079 --> 00:11:43,519
and of course that's you know the whole

332
00:11:41,600 --> 00:11:45,680
topic area of exploitation which again

333
00:11:43,519 --> 00:11:47,279
is not this class

334
00:11:45,680 --> 00:11:49,680
so stack buffer overflows you know

335
00:11:47,279 --> 00:11:51,360
they're very old type of vulnerability

336
00:11:49,680 --> 00:11:53,519
um you know it's not just the smashing

337
00:11:51,360 --> 00:11:55,600
the stack for fun and profit 1996 type

338
00:11:53,519 --> 00:11:58,480
old that I cited before we're talking

339
00:11:55,600 --> 00:12:01,040
like 1980s old and really you know their

340
00:11:58,480 --> 00:12:03,839
citations back to the to the 70s

341
00:12:01,040 --> 00:12:06,399
so you know the very first worm that was

342
00:12:03,839 --> 00:12:09,279
ever released on the proto internet the

343
00:12:06,399 --> 00:12:12,240
arpanet was the morris worm released in

344
00:12:09,279 --> 00:12:14,800
1988 and this was a stack buffer

345
00:12:12,240 --> 00:12:17,519
overflow vulnerability it exploited a

346
00:12:14,800 --> 00:12:19,839
stack overflow in the finger daemon

347
00:12:17,519 --> 00:12:21,760
which would typically run on most unix

348
00:12:19,839 --> 00:12:23,440
systems it was a way of querying

349
00:12:21,760 --> 00:12:24,800
information about other users on the

350
00:12:23,440 --> 00:12:26,720
system

351
00:12:24,800 --> 00:12:29,360
so this has been going on for a very

352
00:12:26,720 --> 00:12:31,040
long time and so the point here is of

353
00:12:29,360 --> 00:12:33,040
saying that's going on for a very long

354
00:12:31,040 --> 00:12:34,880
time is that you know

355
00:12:33,040 --> 00:12:37,440
if everybody's still making these sort

356
00:12:34,880 --> 00:12:39,440
of mistakes in this day and age after

357
00:12:37,440 --> 00:12:40,880
you know well more than 30 years that

358
00:12:39,440 --> 00:12:42,480
means you know it's not fundamentally

359
00:12:40,880 --> 00:12:44,480
the programmer's problem it's the

360
00:12:42,480 --> 00:12:46,399
programming language problem that's why

361
00:12:44,480 --> 00:12:48,320
we're not trying to demonize people who

362
00:12:46,399 --> 00:12:49,760
make these sort of mistakes most people

363
00:12:48,320 --> 00:12:51,839
have never learned anything about them

364
00:12:49,760 --> 00:12:54,240
so how could they know you know not to

365
00:12:51,839 --> 00:12:55,839
have this kind of thing occurring and so

366
00:12:54,240 --> 00:12:58,160
here we're really just trying to focus

367
00:12:55,839 --> 00:13:00,320
on how do we recognize these kind of

368
00:12:58,160 --> 00:13:02,240
mistakes and avoid them

369
00:13:00,320 --> 00:13:06,519
so now let's go look at some real

370
00:13:02,240 --> 00:13:06,519
examples from real vulnerabilities

