1
00:00:00,399 --> 00:00:04,960
now there is a compiler option which you

2
00:00:02,960 --> 00:00:07,600
may have never heard of but which you

3
00:00:04,960 --> 00:00:09,679
may unbeknownst to you be using right

4
00:00:07,600 --> 00:00:12,080
now in order to be getting more secure

5
00:00:09,679 --> 00:00:14,160
code so it's called fortify source and

6
00:00:12,080 --> 00:00:16,560
it was a project started by red hat back

7
00:00:14,160 --> 00:00:18,640
in the early 2000s and the goal was to

8
00:00:16,560 --> 00:00:19,760
basically say well if the compiler can

9
00:00:18,640 --> 00:00:22,320
tell that you're going to buffer

10
00:00:19,760 --> 00:00:23,760
overflow it should probably warn you and

11
00:00:22,320 --> 00:00:25,840
or stop you

12
00:00:23,760 --> 00:00:27,199
either at you know compile time or at

13
00:00:25,840 --> 00:00:30,240
run time

14
00:00:27,199 --> 00:00:32,320
and so the idea was to take things like

15
00:00:30,240 --> 00:00:34,719
memcpy strcpy and all those sort

16
00:00:32,320 --> 00:00:37,840
of APIs that are dangerous and prone to

17
00:00:34,719 --> 00:00:39,680
buffer overflows and have the compiler

18
00:00:37,840 --> 00:00:41,440
insert you know first of all do just

19
00:00:39,680 --> 00:00:42,800
checks to find out whether or not you're

20
00:00:41,440 --> 00:00:44,800
definitely guaranteed going to be

21
00:00:42,800 --> 00:00:46,800
overflowing because it can see you know

22
00:00:44,800 --> 00:00:48,160
using just compile time information that

23
00:00:46,800 --> 00:00:49,840
you know buffer one is bigger than

24
00:00:48,160 --> 00:00:52,079
buffer two and it's going to overflow if

25
00:00:49,840 --> 00:00:54,320
passed into these functions but also to

26
00:00:52,079 --> 00:00:56,559
provide runtime options where these

27
00:00:54,320 --> 00:00:58,399
things like copy strcpy etc will be

28
00:00:56,559 --> 00:01:00,239
replaced with runtime

29
00:00:58,399 --> 00:01:01,440
checking things that will try to see

30
00:01:00,239 --> 00:01:02,559
whether or not there's going to be an

31
00:01:01,440 --> 00:01:04,879
overflow

32
00:01:02,559 --> 00:01:07,119
so there's two main levels of security

33
00:01:04,879 --> 00:01:09,280
that it provides fortify source equals

34
00:01:07,119 --> 00:01:12,799
one and with this compiler option where

35
00:01:09,280 --> 00:01:14,479
available it will do primarily compile

36
00:01:12,799 --> 00:01:16,159
time checks and so that's going to be a

37
00:01:14,479 --> 00:01:18,320
little bit less stringent and of course

38
00:01:16,159 --> 00:01:19,360
you know a lot of times it's controlled

39
00:01:18,320 --> 00:01:22,320
inputs

40
00:01:19,360 --> 00:01:24,080
which are not available at compile time

41
00:01:22,320 --> 00:01:26,799
but if you use

42
00:01:24,080 --> 00:01:28,960
fortify source equals two then the

43
00:01:26,799 --> 00:01:30,799
runtime checks will be more stringent

44
00:01:28,960 --> 00:01:33,200
and then it will try to find out if

45
00:01:30,799 --> 00:01:35,280
there's going to be an overflow

46
00:01:33,200 --> 00:01:38,400
so you opt in through those things like

47
00:01:35,280 --> 00:01:40,159
the dash d fortify source equals one

48
00:01:38,400 --> 00:01:41,759
and I give on the website a whole bunch

49
00:01:40,159 --> 00:01:42,880
of information about you know the

50
00:01:41,759 --> 00:01:44,720
various

51
00:01:42,880 --> 00:01:46,880
about the compilers that support it and

52
00:01:44,720 --> 00:01:48,960
from which version and so forth but it's

53
00:01:46,880 --> 00:01:51,520
also the case that if you're compiling

54
00:01:48,960 --> 00:01:54,640
your code on some unix derivative

55
00:01:51,520 --> 00:01:56,479
macos's Linuxes and bsds it may be the

56
00:01:54,640 --> 00:01:57,920
case that first of all the entire

57
00:01:56,479 --> 00:01:59,600
operating system may have already had

58
00:01:57,920 --> 00:02:02,000
fortify source turned on at the time

59
00:01:59,600 --> 00:02:04,479
that all of its code was compiled and

60
00:02:02,000 --> 00:02:06,799
furthermore the compiler environment

61
00:02:04,479 --> 00:02:09,360
package may be forcing this on it may

62
00:02:06,799 --> 00:02:11,840
have been already setting fortify source

63
00:02:09,360 --> 00:02:14,400
equal to one or two automatically within

64
00:02:11,840 --> 00:02:16,560
your compilation just behind the scenes

65
00:02:14,400 --> 00:02:18,879
so check out the website in order to see

66
00:02:16,560 --> 00:02:23,200
more about the specifics of different

67
00:02:18,879 --> 00:02:23,200
compilers and execution environments

