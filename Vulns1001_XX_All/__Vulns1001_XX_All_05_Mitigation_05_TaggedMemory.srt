1
00:00:00,480 --> 00:00:04,480
now effects blade mitigations like cfi

2
00:00:02,560 --> 00:00:06,240
are just starting to become practical by

3
00:00:04,480 --> 00:00:08,720
virtue of hardware being released that

4
00:00:06,240 --> 00:00:11,519
supports them the exploit mitigation of

5
00:00:08,720 --> 00:00:12,719
tagged memory is a little bit farther

6
00:00:11,519 --> 00:00:14,639
off still

7
00:00:12,719 --> 00:00:17,440
but it's a very important mitigation

8
00:00:14,639 --> 00:00:19,359
because it has applicability to a whole

9
00:00:17,440 --> 00:00:21,119
bunch of different vulnerability classes

10
00:00:19,359 --> 00:00:23,119
as you'll see based on a table that's

11
00:00:21,119 --> 00:00:25,039
going to be on the website so it's very

12
00:00:23,119 --> 00:00:27,599
important for you to know especially if

13
00:00:25,039 --> 00:00:30,320
you're trying to you know focus on

14
00:00:27,599 --> 00:00:32,160
providing high-end security so tech

15
00:00:30,320 --> 00:00:34,559
memory is the notion of providing

16
00:00:32,160 --> 00:00:36,800
fine-grained memory safety where the

17
00:00:34,559 --> 00:00:38,239
idea is that you essentially take and

18
00:00:36,800 --> 00:00:41,040
you have hardware support for a little

19
00:00:38,239 --> 00:00:42,960
bit of a tag or an identifier for

20
00:00:41,040 --> 00:00:45,039
different chunks of memory and then you

21
00:00:42,960 --> 00:00:46,960
know for instance pointers can be made

22
00:00:45,039 --> 00:00:48,800
into smart pointers essentially the

23
00:00:46,960 --> 00:00:51,600
pointer says I'm a pointer that points

24
00:00:48,800 --> 00:00:53,840
at that type that tag of memory and then

25
00:00:51,600 --> 00:00:55,840
if that pointer is ever you know used to

26
00:00:53,840 --> 00:00:57,680
access some other memory somewhere else

27
00:00:55,840 --> 00:00:59,840
then that is treated as a violation by

28
00:00:57,680 --> 00:01:02,000
the hardware and consequently you know

29
00:00:59,840 --> 00:01:04,320
it's prevented thus preventing a whole

30
00:01:02,000 --> 00:01:06,000
bunch of different vulnerability classes

31
00:01:04,320 --> 00:01:08,000
like other things it requires both

32
00:01:06,000 --> 00:01:10,479
compiler and execution environment

33
00:01:08,000 --> 00:01:11,680
support and again if you want to do this

34
00:01:10,479 --> 00:01:13,680
and if you're one of the execution

35
00:01:11,680 --> 00:01:15,040
environment or compiler makers this

36
00:01:13,680 --> 00:01:17,119
would be a good thing for you to start

37
00:01:15,040 --> 00:01:19,520
working on now but as I've already

38
00:01:17,119 --> 00:01:20,960
implied the best forms of this are going

39
00:01:19,520 --> 00:01:23,280
to actually require hardware

40
00:01:20,960 --> 00:01:26,240
modifications in order to work in order

41
00:01:23,280 --> 00:01:28,560
to be actually performant so only you

42
00:01:26,240 --> 00:01:30,400
know very recent like spark change is

43
00:01:28,560 --> 00:01:32,159
not very recent but spark is also not

44
00:01:30,400 --> 00:01:35,520
the most deployed system so with the

45
00:01:32,159 --> 00:01:37,920
spark m7s released in 2015 with silicon

46
00:01:35,520 --> 00:01:40,240
secured memory they did start supporting

47
00:01:37,920 --> 00:01:43,520
tagged memory the more interesting thing

48
00:01:40,240 --> 00:01:46,399
is the arm v 8.5 a memory tagging

49
00:01:43,520 --> 00:01:49,200
extensions which will be you know

50
00:01:46,399 --> 00:01:50,720
hopefully appearing in new chip soon arm

51
00:01:49,200 --> 00:01:52,720
of course you know creates

52
00:01:50,720 --> 00:01:55,200
specifications and they license

53
00:01:52,720 --> 00:01:57,360
Intellectual property to other

54
00:01:55,200 --> 00:01:59,200
companies to build based on so

55
00:01:57,360 --> 00:02:00,880
consequently it's up to different chip

56
00:01:59,200 --> 00:02:02,880
makers to decide whether they're going

57
00:02:00,880 --> 00:02:04,640
to incorporate this or not so again you

58
00:02:02,880 --> 00:02:06,880
know if you're someone who's working on

59
00:02:04,640 --> 00:02:09,200
the low level stuff perhaps if you're a

60
00:02:06,880 --> 00:02:10,959
firmware person trying to say like i

61
00:02:09,200 --> 00:02:12,560
want to build you know extremely secure

62
00:02:10,959 --> 00:02:14,560
firmware you might have to talk to your

63
00:02:12,560 --> 00:02:16,400
hardware architects and say hey we

64
00:02:14,560 --> 00:02:19,760
should you know try to go incorporate

65
00:02:16,400 --> 00:02:22,239
this rmv 8.5 a memory tugging extension

66
00:02:19,760 --> 00:02:24,160
support into our future chips like other

67
00:02:22,239 --> 00:02:26,800
exploit mitigations again it typically

68
00:02:24,160 --> 00:02:28,800
starts in academia and moves its way

69
00:02:26,800 --> 00:02:30,560
into industry and so the thing that has

70
00:02:28,800 --> 00:02:33,680
been you know pushing this most

71
00:02:30,560 --> 00:02:36,160
productively forward since about 2010 is

72
00:02:33,680 --> 00:02:38,720
capability hardware enhanced risk

73
00:02:36,160 --> 00:02:41,519
instructions or cherry cherry has you

74
00:02:38,720 --> 00:02:42,560
know got the lion's share of the mind

75
00:02:41,519 --> 00:02:44,239
share

76
00:02:42,560 --> 00:02:46,800
in terms of you know what people

77
00:02:44,239 --> 00:02:48,400
actually want to adopt so they are the

78
00:02:46,800 --> 00:02:52,560
ones who've been you know pushing for

79
00:02:48,400 --> 00:02:54,800
this the arm v 8.5 a specification you

80
00:02:52,560 --> 00:02:56,959
know definitely supports them there is

81
00:02:54,800 --> 00:02:59,519
prototype hardware currently available

82
00:02:56,959 --> 00:03:00,720
called morelo and that can actually work

83
00:02:59,519 --> 00:03:03,599
specifically

84
00:03:00,720 --> 00:03:05,920
using this prototype hardware with the

85
00:03:03,599 --> 00:03:07,440
cherry instruction set in order to

86
00:03:05,920 --> 00:03:09,840
achieve this

87
00:03:07,440 --> 00:03:11,200
what they call capabilities in order to

88
00:03:09,840 --> 00:03:13,360
achieve you know fine-grained memory

89
00:03:11,200 --> 00:03:14,959
tagging and memory safety additionally

90
00:03:13,360 --> 00:03:17,040
you can you know read some research by

91
00:03:14,959 --> 00:03:19,680
Microsoft about you know using cherry as

92
00:03:17,040 --> 00:03:21,920
well so again you can't actually opt

93
00:03:19,680 --> 00:03:23,680
into anything specifically today except

94
00:03:21,920 --> 00:03:25,440
spark and spark is not the most common

95
00:03:23,680 --> 00:03:27,760
thing but I really wanted you to know

96
00:03:25,440 --> 00:03:29,440
about this if you're interested in high

97
00:03:27,760 --> 00:03:32,400
security platforms

98
00:03:29,440 --> 00:03:33,920
because it will be coming soon and

99
00:03:32,400 --> 00:03:36,000
you know it will become the type of

100
00:03:33,920 --> 00:03:37,920
thing that can be opted into and as

101
00:03:36,000 --> 00:03:39,200
usual the website will have the details

102
00:03:37,920 --> 00:03:41,599
about what's available now and the

103
00:03:39,200 --> 00:03:44,640
website will be updated as new things

104
00:03:41,599 --> 00:03:44,640
become available

